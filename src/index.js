const Discord = require("discord.js");
const fs = require("fs");
const bot = new Discord.Client();
const config = require("./config.json");
bot.config = config;
bot.logsEmbed = new Discord.RichEmbed()
    .setColor('RANDOM')
    .setTimestamp()
    .setDescription("")
    .setTitle("")
    .fields = [];
fs.readdir("./events/", (err, files) => {
    if (err) return console.error(err);
    files.forEach(file => {
        const event = require((`./events/${file}`)
            .trim());
        let eventName = file.split(".")[0];
        bot.on(eventName, event.bind(null, bot));
    });
});
bot.commands = new Discord.Collection();
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));
for (const file of commandFiles) {
	const command = require(`./commands/${file}`);
	bot.commands.set(command.name, command);
}
bot.login(config.token);
