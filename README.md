# FederalBot: Discord's Democracy

A bot that was specifically made for my server, "United Discord Democracy", or U.D.D for short. It's a democratic server, with the people put first. This bot is the basis for the entire system.

This server is based off of "Reborn", which you can find the bot for [here.](https://github.com/Lunerr/reborn)

This is not a school project, or anything like that. Just wanted to make a good, unique bot for Discord.

Want to join? Join [here.](https://discord.gg/M672RYY)

# Useful Stuff

### PLEASE READ THESE BEFORE SUBMITTING ANY PR'S OR CODE ISSUE REPORTS!

discord.js [guide](https://www.discordjs.guide) ([github](https://github.com/discordjs/guide))

discord.js bot [guide](https://www.anidiots.guide) ([github](https://github.com/AnIdiotsGuide/discordjs-bot-guide))

Javascript [reference](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference) and [tutorials](https://developer.mozilla.org/en-US/docs/Web/JavaScript/) ([github](https://github.com/mdn))

discord.js [reference](https://discord.js.org) ([github](https://www.github.com/discordjs/discord.js))

API [reference](https://www.discordapp.com/developers/docs/intro) ([github](https://github.com/discordapp/discord-api-docs))

Looked through those and still don't understand? Here are your options:
* Contact me on Discord (I am bug/finnarf on the server)
* Look up other guides
* Go to dedicated JS servers

Don't go and complain to me if you don't know how to get these to work. Not my fault.

Also, it may be worth checking out my [JS demonstrations](https://github.com/spergmoment/js-demonstrations) repo.
# PR's and Issues

Only make a PR if:
* You know where the problem is.
* You know how to fix it.
* You are 100% sure that what you are doing will help the bot.

OR, you have an approved suggestion.

Only make an issue if:
* You found a code issue, bug in the bot, or have a suggestion.
* For code issues:
  * You don't meet the requirements for the PR.
  * You know exactly where the issue is.
* For bugs:
  * You know what the bug is.
  * You know what thing this bug prevents from working.
  * The bug is a big issue (Don't worry about this; I'll tell you).
* For suggestions:
  * You are 100% sure what you are doing.
  * Your suggestion will help the bot.
  * I am able to implement this (I'll tell you if I can).

If you don't follow these, I may temporarily lock you, or warn you, depending on the severity.
# FAQ's that are not frequently asked at all

Q: Can I help make this?
* A: Yes, but only if you have joined the server and are an experienced-enough JS/discord.js dev. 
* Q: But what does "experienced-enough" mean? Is there any concrete answer?
* A: No. I will ask you to make something simple, then gradually go up in difficulty to see your spatial reasoning skills/how "advanced" you are at JS. I will also ask you to make me a simple bot in discord.js or discord.eris, whether you aren't able to do any JS besides discord.js and .eris, or you passed enough base JS tests. Contact me on Discord for more info.
* Q: Okay, I've done that. What happens now?
* A: You get a special role on the Discord server. I will not grant write access in the repo until you have proven to be a great help to the bot. Make PR's and issues frequently, and you can gain write access!

Q: Why did you make this?
* A: Your guess is as good as mine, but from what I remember it was to improve my JS skills as a whole.

Q: What's the point?
* A: Democracy. It doesn't work in the real world because free speech was a mistake, but works great on Discord (as long as people on the server aren't retarded).

Q: Do you have other bots?
* A: Yup! I released a few already. I may put out some more, but it depends on anything, really.

Q: Can I add this to my own server?
* A: Absolutely! Contact me on discord (shown at the top section) and I will tell you how you can set up the bot for your own server.

Ask me any questions on the discord server and I will happily answer them!
# Extra Info

Version currently running: v1.1.5

Running on: Arch linux 5.4.15, LXDE desktop env

Currently working on: SQL DBs, Performance, Money System

Current prefix: ;
